/*
 * Copyright (C) MCCoding - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 * Please contact MCCoding for exclusive permissions in regards to this file.
 *
 * Use of this file, in any form or degree, is prohibitted outside the terms and conditions
 * of MCCoding. Any use of the code and/or contents contained within this file
 * without exclusive permission from MCCoding is strictly prohibitted.
 *
 * Use of this file, in any form or degree, is permitted when all parties involved are
 * considered, under the active current owner of MCCoding, MCCoding
 * employees or developers. Other projects, code, or mediums which use and/or host this
 * file, in it's entirety or in part, must be considered active and legal under the
 * active current owner of MCCoding.
 *
 * If a project, host, or otherwise entity using this file in part or in full, to any degree
 * is terminated under the terms and/or contract provided by the current active owner of
 * MCCoding, then the abusing project, host, or otherwise entity must terminate
 * the use of this file, in full or in part, regardless of permission. In order for a
 * project, host, or otherwise entity to regain permission to use this file, in whole or
 * in part, there must be written permission from the current active owner of MCCoding.
 *
 * For questions or concerns, please contact MCCoding directly at the following
 * email address: support@mccoding.us.
 *
 * This file, in whole or in part, and it's users are to be considered protected from
 * 2014 until the termination of this notice as specified with written permission
 * from MCCoding.
 */

package us.mccoding.PlayerList.commands;


import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class CommandOnline implements CommandExecutor
{


    @Override
    public boolean onCommand(CommandSender cs, Command command, String s, String[] args)
    {
        StringBuilder sb = new StringBuilder();

        int index = 0;
        for (Player p : Bukkit.getOnlinePlayers())
        {
            index++;
            if (p.hasPermission("playerlist.staff"))
            {
                sb.append(p.getName()).append(index < Bukkit.getOnlinePlayers().length ? ", " : "");
            }
        }


        cs.sendMessage("§6=====================================================");
        cs.sendMessage("");
        cs.sendMessage("§cThere are currently §b" + Bukkit.getOnlinePlayers().length + " / " + Bukkit.getMaxPlayers() + " §cplayers online");
        cs.sendMessage("");
        cs.sendMessage("§7Online Staff: §a" + (sb.toString().length() > 0 ? sb.toString() : "§cNone."));
        cs.sendMessage("");
        cs.sendMessage("§6=====================================================");

        return false;
    }
}
