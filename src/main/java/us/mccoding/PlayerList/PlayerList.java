/*
 * Copyright (C) MCCoding - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 * Please contact MCCoding for exclusive permissions in regards to this file.
 *
 * Use of this file, in any form or degree, is prohibitted outside the terms and conditions
 * of MCCoding. Any use of the code and/or contents contained within this file
 * without exclusive permission from MCCoding is strictly prohibitted.
 *
 * Use of this file, in any form or degree, is permitted when all parties involved are
 * considered, under the active current owner of MCCoding, MCCoding
 * employees or developers. Other projects, code, or mediums which use and/or host this
 * file, in it's entirety or in part, must be considered active and legal under the
 * active current owner of MCCoding.
 *
 * If a project, host, or otherwise entity using this file in part or in full, to any degree
 * is terminated under the terms and/or contract provided by the current active owner of
 * MCCoding, then the abusing project, host, or otherwise entity must terminate
 * the use of this file, in full or in part, regardless of permission. In order for a
 * project, host, or otherwise entity to regain permission to use this file, in whole or
 * in part, there must be written permission from the current active owner of MCCoding.
 *
 * For questions or concerns, please contact MCCoding directly at the following
 * email address: support@mccoding.us.
 *
 * This file, in whole or in part, and it's users are to be considered protected from
 * 2014 until the termination of this notice as specified with written permission
 * from MCCoding.
 */

package us.mccoding.PlayerList;


import org.bukkit.plugin.java.JavaPlugin;
import us.mccoding.PlayerList.commands.CommandOnline;


public class PlayerList extends JavaPlugin
{


    private static PlayerList instance;




    @Override
    public void onEnable()
    {
        instance = this;

        getCommand("online").setExecutor(new CommandOnline());
    }




    @Override
    public void onDisable()
    {
        instance = null;
    }




    public static PlayerList getInstance()
    {
        return instance;
    }
}
